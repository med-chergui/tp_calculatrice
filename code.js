var operation = {
    firstNum : '',
    secengNum : '',
    ops : '',
    ret :function (){
        var res = 0;
        switch (this.ops) {
            case '+':
                res = (+(this.firstNum) + +(this.secengNum));
                break;
            case '-':
                res = (this.firstNum - this.secengNum);
                break;
            case '*':
                res = (this.firstNum * this.secengNum);
                break;
                case '/':
                    if (this.secengNum == 0){
                        res = 'math erreur'
                    }else{
                        res = (this.firstNum / this.secengNum);
                    }
                    break;
                case '^':
                    res = (Math.pow(this.firstNum, this.secengNum));
                    break;
                case '√':
                    if (this.secengNum >= 0){
                        res = (this.firstNum * Math.sqrt(this.secengNum));
                    }
                    else{
                        res = 'math erreur';
                    }
                    break;
            default:
                res = (+(this.firstNum) + +(this.secengNum));
                break;
        }
        return(res);
    }
}

var box = document.getElementById('box');
var last = document.getElementById('last_operation_history');
var lastOp = '';

function button_number(n){
    if (Number(n) == n || n == '.'){
        if (operation.ops == ''){
            operation.firstNum += n;
            box.innerText = operation.firstNum;
        }else{
            operation.secengNum += n;
            box.innerText = operation.secengNum;
        }
    }
    else if (n == '='){
        box.innerText = (operation.ret());
        lastOp = `${operation.firstNum} ${operation.ops} ${operation.secengNum} = ${operation.ret()}`;
        operation.firstNum = '';
        operation.secengNum = '';
        operation.ops = "";
        last.innerText = lastOp;
    }
    else {
        operation.ops = n;
        box.innerText = n;
    }
    console.log(operation);
}


function backspace_remove(){

    if (operation.ops == ''){
        operation.firstNum = (operation.firstNum.slice(0,-1)); 
        box.innerText = operation.firstNum
    }else{
        operation.secengNum = (operation.secengNum.slice(0,-1)); 
        box.innerText = operation.firstNum
    }

    console.log(operation);
}

function division_one(){
    operation.firstNum = '1';
    operation.ops = '/'
    box.innerText = '1/';
}

function power_of(){
    button_number('^');
}

function square_root(){
    if (operation.firstNum == ''){
        operation.firstNum = '1'
    }
    button_number('√');
}

function plus_minus(){
    if (operation.ops == ''){
        if (operation.firstNum == ''){
            operation.firstNum = '-'
        }else{
            operation.firstNum = (operation.firstNum * -1).toString()
        }
        box.innerText = operation.firstNum;
    } else {
        if (operation.secengNum == ''){
            operation.secengNum = '-'
        }else{
            operation.secengNum = (operation.secengNum * -1).toString()
        }
        box.innerText = operation.secengNum;
    }
}

function button_clear(){
    operation.firstNum = '';
    operation.secengNum = '';
    operation.ops = "";
    box.innerText = '';
}

function clear_entry(){
    operation.firstNum = '';
    operation.secengNum = '';
    operation.ops = "";
    last.innerText = '';
    box.innerText = '';
}

